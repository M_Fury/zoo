import Zoo.AnimalManager;

public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid parameters. Type the animals' filename as an argument.");
            return;
        }

        AnimalManager.printAnimalsInfo(args[0]);
    }
}
