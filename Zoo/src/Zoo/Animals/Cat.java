package Zoo.Animals;

import Zoo.SimpleAnimal;

public class Cat extends SimpleAnimal {
    public Cat() {
        name = "Cat";
        sound = "Miao";
    }
}
