package Zoo.Animals;

import Zoo.SimpleAnimal;

public class Duck extends SimpleAnimal {
    public Duck() {
        name = "Duck";
        sound = "Ga ga";
    }
}
