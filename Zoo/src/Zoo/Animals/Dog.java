package Zoo.Animals;

import Zoo.SimpleAnimal;

public class Dog extends SimpleAnimal {
    public Dog() {
        name = "Dog";
        sound = "How";
    }
}
