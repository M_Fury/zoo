package Zoo;

import Zoo.Exceptions.AnimalGenerationException;
import Zoo.Exceptions.AnimalNotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class AnimalManager {
    private final static String PROPERTIES_FILENAME = "names.properties";

    public static void printAnimalsInfo(final String animalsFilename) {
        File animalsFile = new File(animalsFilename);

        try (Scanner fileReader = new Scanner(animalsFile)) {
            AnimalFactory factory = new AnimalFactory();
            factory.loadExistingNames(PROPERTIES_FILENAME);

            while (fileReader.hasNext()) {
                printAnimalInfo(fileReader.next(), factory);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printAnimalInfo(String animalName, AnimalFactory factory) {
        try {
            Animal newAnimal = factory.generateAnimal(animalName);
            newAnimal.printName();
            newAnimal.printSound();
        } catch (AnimalNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (AnimalGenerationException e) {
            e.printStackTrace();
        }
    }
}
