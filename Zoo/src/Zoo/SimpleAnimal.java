package Zoo;

public class SimpleAnimal implements Animal {
    protected String name;
    protected String sound;

    public void printName() {
        System.out.println(this.name);
    }

    public void printSound() {
        System.out.println(this.sound);
    }
}