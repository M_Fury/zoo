package Zoo;

import Zoo.Exceptions.AnimalGenerationException;
import Zoo.Exceptions.AnimalNotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Optional;
import java.util.Properties;

public class AnimalFactory {
    private final Properties existingNames = new Properties();

    public void loadExistingNames(String propertiesFilename) throws IOException {
        InputStream namesInputStream = AnimalManager.class.getClassLoader()
                .getResourceAsStream(propertiesFilename);

        if (namesInputStream == null) {
            throw new FileNotFoundException(
                    "The property file \"" + propertiesFilename + "\" wasn't found.");
        }

        existingNames.load(namesInputStream);

        namesInputStream.close();
    }

    public Animal generateAnimal(String animalName) throws AnimalGenerationException {
        Optional<String> className = Optional.ofNullable(
                this.existingNames.getProperty(animalName));

        if (className.isEmpty()) {
            throw new AnimalNotFoundException(
                    "The animal \"" + animalName + "\" wasn't found.");
        }

        try {
            return createAnimal(className.get());
        } catch (ReflectiveOperationException | NoClassDefFoundError e) {
            throw new AnimalGenerationException(
                    "Failed to generate the animal \"" + animalName + "\".", e);
        }
    }

    private static Animal createAnimal(String className) throws ReflectiveOperationException {
        Class<? extends Animal> animalClass =
                Class.forName(className).asSubclass(Animal.class);
        Constructor<? extends Animal> animalConstructor = animalClass.getConstructor();

        return animalConstructor.newInstance();
    }
}
