package Zoo.Exceptions;

public class AnimalGenerationException extends Exception {
    public AnimalGenerationException(String errorMessage) {
        super(errorMessage);
    }

    public AnimalGenerationException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
