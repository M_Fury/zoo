package Zoo.Exceptions;

public class AnimalNotFoundException extends AnimalGenerationException {
    public AnimalNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
